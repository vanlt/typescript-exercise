"use strict";
exports.__esModule = true;
var manage_employee_1 = require("./manage-employee");
var manage_employee_2 = require("./manage-employee");
var manage_employee_3 = require("./manage-employee");
var manage_employee_4 = require("./manage-employee");
var readlineSync = require('readline-sync');
function title() {
    console.log("----------------------------------------------------------");
    console.log("1.Nhập danh sách nhân viên | 2.Xem nhân viên | 3.Xem lương");
    var selected = parseInt(readlineSync.question("Nhập lựa chọn: "));
    return selected;
}
function input() {
    console.log("-----------------------------------------------------------------");
    console.log("Loại nhân viên: 1. NV sản xuất | 2. NV công nhật | 3. NV quản lý ");
    var type = parseInt(readlineSync.question("Chọn:"));
    switch (type) {
        case 1: {
            var prod_staff = new manage_employee_1.Production_staff();
            prod_staff.input_staff();
            manage_employee_4.Staff.list_staff.push(prod_staff);
            break;
        }
        case 2: {
            var daytaler = new manage_employee_2.Daytaler();
            daytaler.input_staff();
            manage_employee_4.Staff.list_staff.push(daytaler);
            break;
        }
        case 3: {
            var manage_staff = new manage_employee_3.Manager_staff();
            manage_staff.input_staff();
            manage_employee_4.Staff.list_staff.push(manage_staff);
            break;
        }
        default: {
            console.log("Lựa chọn không hợp lệ, vui lòng nhập lại");
            break;
        }
    }
}
function choice() {
    switch (title()) {
        case 1: {
            var number = readlineSync.question("số lượng: ");
            for (var i = 0; i < number; i++) {
                input();
            }
            continute();
            break;
        }
        case 2: {
            var count_1 = 0;
            manage_employee_4.Staff.list_staff.forEach(function (element) {
                count_1++;
                console.log("[" + count_1 + "]");
                element.output();
            });
            continute();
            break;
        }
        case 3: {
            var sum_salary_1 = 0;
            var max_1 = manage_employee_4.Staff.list_staff[0];
            var min_1 = manage_employee_4.Staff.list_staff[0];
            manage_employee_4.Staff.list_staff.forEach(function (element) {
                sum_salary_1 = sum_salary_1 + element.getSalary();
                if (element.getSalary() > max_1.getSalary())
                    max_1 = element;
                if (min_1.getSalary() > element.getSalary())
                    min_1 = element;
            });
            console.log("Tổng lương của các nhân viên trong công ty: " + sum_salary_1);
            console.log("Nhân viên có lương cao nhất: ");
            max_1.output();
            console.log("Nhân viên có lương thấp nhất: ");
            min_1.output();
            continute();
            break;
        }
        default: {
            console.log("Lựa chọn không hợp lệ, vui lòng nhập lại");
            continute();
            break;
        }
    }
}
function continute() {
    var selected = readlineSync.question("Tiếp tục (y/n)");
    while (selected !== "y" && selected !== "n") {
        selected = readlineSync.question("Lựa chọn không hợp lệ, vui lòng nhập lại, Tiếp tục (y/n)");
    }
    if (selected === "y") {
        choice();
    }
    else
        return;
}
choice();
