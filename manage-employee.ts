
abstract class Staff {
    readlineSync = require('readline-sync');
    protected name: string;
    protected dob: Date;
    protected address: string;
    protected salary: number;
    protected type: string;
    static list_staff: any[]=[];

    constructor(name?: string, dob?:Date, address?:string, salary?:number,type?:string) {
       this.name=!name?"":name;
       this.dob=!dob?new Date(0):dob;
       this.address=!address?"":address; 
       this.salary=!salary?0:salary; 
       this.type=!type?"":type;
    }

    protected setName(name:string):void{
       this.name=name;
    }
    protected setDoB(dob:Date):void{
        this.dob=dob;
    }
    protected setAddress(address:string):void{
        this.address=address;
    }
    protected setSalary(salary:number):void{
        this.salary=salary;
    }
    protected getName():string{
        return this.name;
     }
    protected getDoB():Date{
         return this.dob;
     }
    protected getAddress():string{
         return this.address;
     }
    
    protected getSalary():number{
        return this.salary;
    }    
    protected setType(type:string):void{
        this.type=type;
    }
   
   protected getType():string{
       return this.type;
   }    

    public input_staff():void{
        this.name= this.readlineSync.question("Tên: ");      
        this.dob= this.readlineSync.question("Ngày sinh: ");
        this.address= this.readlineSync.question("Địa chỉ: ");
    }
    public output():void{
        console.log("Tên: "+this.getName());
        console.log("Ngày sinh: "+this.getDoB());
        console.log("Địa chỉ: "+this.getAddress());
        console.log("Lương: "+this.getSalary());
        console.log("Loại NV: "+this.getType());
    }
    protected abstract compute_salary():void;

}

class Production_staff extends Staff{

    private prod_quantity:number;
    constructor(name?: string, dob?:Date, address?:string,prod_quantity?:number,salary?:number,type:string="sản xuất") {
        super(name,dob,address,salary,type);
        this.prod_quantity= !prod_quantity?0:prod_quantity;
    }

    protected setprod_quantity():string{
        return this.address;
    }
   
   protected getSalary():number{
       return this.salary;
   }    
    protected getProd_quantity():number{
        return this.prod_quantity;
    }
    protected setType(type:string):void{
        super.setType("sản xuất");
    }
    protected compute_salary():number{
        this.salary= this.prod_quantity * 20000;
        return this.salary;
    }
    
    public input_staff():void{
        super.input_staff();
        this.prod_quantity= this.readlineSync.question("Số  sản phẩm: ");        
        this.compute_salary();
    }
    public output():void{
        super.output();
    }

}

class Daytaler extends Staff{

    private workdays_number:number;
    constructor(name?: string, dob?:Date, address?:string,workdays_number?:number, salary?:number,type:string="công nhật") {
        super(name,dob,address,salary,type);
        this.workdays_number= !workdays_number?0:workdays_number;
    }
    protected getWorkdays_number():number{
        return this.workdays_number;
    }
    protected compute_salary():number{
        this.salary= this.workdays_number * 50000;
        return this.salary;
    }
    protected setType(type:string):void{
        super.setType("công nhật");
    }
    public input_staff():void{
        super.input_staff();
        this.workdays_number= this.readlineSync.question("Ngày công: ");        
        this.compute_salary();
    }
    public output():void{
        super.output();
    }

}
class Manager_staff extends Staff{

    private basic_salary:number;
    constructor(name?: string, dob?:Date, address?:string,basic_salary?:number,salary?:number,type:string="quản lý") {
        super(name,dob,address,salary,type);
        this.basic_salary= !basic_salary?0:basic_salary;
    }
    protected getWorkdays_number():number{
        return this.basic_salary;
    }
    protected setType(type:string):void{
        super.setType("quản lý");
    }
    protected compute_salary():number{
        let factor= this.readlineSync.question("Hệ số: ");        
        this.salary= this.basic_salary * factor;
        return this.salary;
    }
    
    public input_staff():void{
        super.input_staff();
        this.basic_salary= this.readlineSync.question("Lương cơ bản: ");        
        this.compute_salary();
    }
    public output():void{
        super.output();
    }

}

export{Production_staff};
export{Daytaler};
export{Manager_staff};
export{Staff};


