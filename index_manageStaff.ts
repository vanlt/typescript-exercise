import{Production_staff} from "./manage-employee";
import{Daytaler}  from "./manage-employee";
import{Manager_staff}  from "./manage-employee";
import{Staff}  from "./manage-employee";
let readlineSync = require('readline-sync');
  
function title():number{
    console.log("----------------------------------------------------------");
    console.log("1.Nhập danh sách nhân viên | 2.Xem nhân viên | 3.Xem lương");
    let selected:number= parseInt(readlineSync.question("Nhập lựa chọn: ")); 
    return selected;      
}

function input():void{
    console.log("-----------------------------------------------------------------");
    console.log("Loại nhân viên: 1. NV sản xuất | 2. NV công nhật | 3. NV quản lý ");
    let type:number= parseInt(readlineSync.question("Chọn:"));
    switch(type){
        case 1: {  
            let prod_staff = new Production_staff();
            prod_staff.input_staff();
            Staff.list_staff.push(prod_staff);
            break;
        }
        case 2: {
            let daytaler= new Daytaler();
            daytaler.input_staff();
            Staff.list_staff.push(daytaler);
            break;
        }
        case 3: {
            let manage_staff= new Manager_staff();
            manage_staff.input_staff();
            Staff.list_staff.push(manage_staff);
            break;
        }
        default: {
            console.log("Lựa chọn không hợp lệ, vui lòng nhập lại");
            break;
        }
    }
} 

function choice():void{
    switch(title()){
        case 1: {
            let number:number = readlineSync.question("số lượng: ");
            for(let i: number=0; i<number; i++){
                input();
            }
            continute();
            break;
        }
        case 2: {
            let count:number=0;
            Staff.list_staff.forEach(element => {
                count++
                console.log("["+count+"]");
                element.output();
            });
            continute();
            break;
        }
        case 3: {
            let sum_salary:number=0;
            let max:any=Staff.list_staff[0];
            let min:any=Staff.list_staff[0];
            Staff.list_staff.forEach(element => {
                sum_salary=sum_salary + element.getSalary();
                if(element.getSalary() > max.getSalary())
                    max = element;
                if(min.getSalary() > element.getSalary())
			        min = element;
            });
            console.log("Tổng lương của các nhân viên trong công ty: " + sum_salary);
            console.log("Nhân viên có lương cao nhất: ");
            max.output();
            console.log("Nhân viên có lương thấp nhất: ");
            min.output();
            continute();
            break;
        }
        default: {
            console.log("Lựa chọn không hợp lệ, vui lòng nhập lại");
            continute();
            break;
        }
    }
}

function continute():void{
    let selected:string = readlineSync.question("Tiếp tục (y/n)"); 
     while (selected !=="y" && selected !=="n"){
        selected = readlineSync.question("Lựa chọn không hợp lệ, vui lòng nhập lại, Tiếp tục (y/n)");
     }  
    if(selected==="y"){
        choice();
    }  else return;
}

choice();