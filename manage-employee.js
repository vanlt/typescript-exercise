"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var Staff = /** @class */ (function () {
    function Staff(name, dob, address, salary, type) {
        this.readlineSync = require('readline-sync');
        this.name = !name ? "" : name;
        this.dob = !dob ? new Date(0) : dob;
        this.address = !address ? "" : address;
        this.salary = !salary ? 0 : salary;
        this.type = !type ? "" : type;
    }
    Staff.prototype.setName = function (name) {
        this.name = name;
    };
    Staff.prototype.setDoB = function (dob) {
        this.dob = dob;
    };
    Staff.prototype.setAddress = function (address) {
        this.address = address;
    };
    Staff.prototype.setSalary = function (salary) {
        this.salary = salary;
    };
    Staff.prototype.getName = function () {
        return this.name;
    };
    Staff.prototype.getDoB = function () {
        return this.dob;
    };
    Staff.prototype.getAddress = function () {
        return this.address;
    };
    Staff.prototype.getSalary = function () {
        return this.salary;
    };
    Staff.prototype.setType = function (type) {
        this.type = type;
    };
    Staff.prototype.getType = function () {
        return this.type;
    };
    Staff.prototype.input_staff = function () {
        this.name = this.readlineSync.question("Tên: ");
        this.dob = this.readlineSync.question("Ngày sinh: ");
        this.address = this.readlineSync.question("Địa chỉ: ");
    };
    Staff.prototype.output = function () {
        console.log("Tên: " + this.getName());
        console.log("Ngày sinh: " + this.getDoB());
        console.log("Địa chỉ: " + this.getAddress());
        console.log("Lương: " + this.getSalary());
        console.log("Loại NV: " + this.getType());
    };
    Staff.list_staff = [];
    return Staff;
}());
exports.Staff = Staff;
var Production_staff = /** @class */ (function (_super) {
    __extends(Production_staff, _super);
    function Production_staff(name, dob, address, prod_quantity, salary, type) {
        if (type === void 0) { type = "sản xuất"; }
        var _this = _super.call(this, name, dob, address, salary, type) || this;
        _this.prod_quantity = !prod_quantity ? 0 : prod_quantity;
        return _this;
    }
    Production_staff.prototype.setprod_quantity = function () {
        return this.address;
    };
    Production_staff.prototype.getSalary = function () {
        return this.salary;
    };
    Production_staff.prototype.getProd_quantity = function () {
        return this.prod_quantity;
    };
    Production_staff.prototype.setType = function (type) {
        _super.prototype.setType.call(this, "sản xuất");
    };
    Production_staff.prototype.compute_salary = function () {
        this.salary = this.prod_quantity * 20000;
        return this.salary;
    };
    Production_staff.prototype.input_staff = function () {
        _super.prototype.input_staff.call(this);
        this.prod_quantity = this.readlineSync.question("Số  sản phẩm: ");
        this.compute_salary();
    };
    Production_staff.prototype.output = function () {
        _super.prototype.output.call(this);
    };
    return Production_staff;
}(Staff));
exports.Production_staff = Production_staff;
var Daytaler = /** @class */ (function (_super) {
    __extends(Daytaler, _super);
    function Daytaler(name, dob, address, workdays_number, salary, type) {
        if (type === void 0) { type = "công nhật"; }
        var _this = _super.call(this, name, dob, address, salary, type) || this;
        _this.workdays_number = !workdays_number ? 0 : workdays_number;
        return _this;
    }
    Daytaler.prototype.getWorkdays_number = function () {
        return this.workdays_number;
    };
    Daytaler.prototype.compute_salary = function () {
        this.salary = this.workdays_number * 50000;
        return this.salary;
    };
    Daytaler.prototype.setType = function (type) {
        _super.prototype.setType.call(this, "công nhật");
    };
    Daytaler.prototype.input_staff = function () {
        _super.prototype.input_staff.call(this);
        this.workdays_number = this.readlineSync.question("Ngày công: ");
        this.compute_salary();
    };
    Daytaler.prototype.output = function () {
        _super.prototype.output.call(this);
    };
    return Daytaler;
}(Staff));
exports.Daytaler = Daytaler;
var Manager_staff = /** @class */ (function (_super) {
    __extends(Manager_staff, _super);
    function Manager_staff(name, dob, address, basic_salary, salary, type) {
        if (type === void 0) { type = "quản lý"; }
        var _this = _super.call(this, name, dob, address, salary, type) || this;
        _this.basic_salary = !basic_salary ? 0 : basic_salary;
        return _this;
    }
    Manager_staff.prototype.getWorkdays_number = function () {
        return this.basic_salary;
    };
    Manager_staff.prototype.setType = function (type) {
        _super.prototype.setType.call(this, "quản lý");
    };
    Manager_staff.prototype.compute_salary = function () {
        var factor = this.readlineSync.question("Hệ số: ");
        this.salary = this.basic_salary * factor;
        return this.salary;
    };
    Manager_staff.prototype.input_staff = function () {
        _super.prototype.input_staff.call(this);
        this.basic_salary = this.readlineSync.question("Lương cơ bản: ");
        this.compute_salary();
    };
    Manager_staff.prototype.output = function () {
        _super.prototype.output.call(this);
    };
    return Manager_staff;
}(Staff));
exports.Manager_staff = Manager_staff;
